#import package form sparksession
from pyspark.sql import SparkSession
#create spark app name
spark = SparkSession.builder.appName("Startup").getOrCreate()
#read csv file store into spark dataframe
startup=spark.read.csv('/home/hitesh/Desktop/solution/startup.csv',header=True)
#read parquet file into saprk dataframe
consumerInternet=spark.read.parquet('/home/hitesh/Desktop/solution/consumerInternet.parquet')
# it will concat the two dataframe into 1 dataframe
df=startup.union(consumerInternet)
#provide the temporary table name to the dataframe
df.registerTempTable("startups")



# How many startups are there in Pune City?
startupCount=spark.sql(('select count(distinct Startup_Name) as puneStartupCount from startups where City=\'Pune\''))
#it will write into csv file
startupCount.write.save(path='/tmp/output/PuneCount',format='csv',mode='Overwrite')


# How many startups in Pune got their Seed/ Angel Funding?
startupCount1=spark.sql(('select count(*) startup_Cnt_Pune_Fund from startups where City=\'Pune\' and InvestmentnType="Seed/ Angel Funding"'))
#it will write into csv file
startupCount1.write.save(path='/tmp/output/PuneCountFunding',format='csv',mode='Overwrite')


# What is the total amount raised by startups in Pune City? Hint - use regex_replace to get rid of null
amt_raised=spark.sql(('''select sum(regexp_replace(Amount_in_USD, '[^0-9]+', 0)) punefund from startups where City="Pune"'''))
#it will write into csv file
amt_raised.write.save(path='/tmp/output/puneAmntRaised',format='csv',mode='Overwrite')


# What are the top 5 Industry_Vertical which has the highest number of startups in India?
top5industry = spark.sql('''select Industry_Vertical, count(Startup_Name) sup_count from startups group by Industry_Vertical order by sup_count desc limit 5''')
#it will write into csv file
top5industry.write.save(path='/tmp/output/top5industry',format='csv',mode='Overwrite')

# Find the top Investor(by amount) of each year.
top_investor = spark.sql('''select Investors_Name, (CAST(regexp_replace(SUBSTRING(Date,6,10), '[^0-9]*', '') AS BIGINT)) date, SUM(CAST(regexp_replace(Amount_in_USD, '[^0-9]*', '') AS BIGINT)) amount from startups where Investors_Name!="N/A" group by Investors_Name, date ORDER BY date desc ,amount desc''')
top_investor.write.save(path='/tmp/output/TopInvestor',format='csv',mode='Overwrite')
# top_investor.show()

#######
#Bonus Question
# Find the top startup(by amount raised) from each city?
topstartups = spark.sql('''select City, SUM(CAST(regexp_replace(Amount_in_USD, '[^0-9]*', '') AS BIGINT)) total from startups where City!="nan" group by City order by total desc''')
topstartups.write.save(path='/tmp/output/Topstartups',format='csv',mode='Overwrite')

#Which SubVertical had the highest growth(in number of startups) over the years?
highestGrowthstartup = spark.sql("select SubVertical,COUNT(Startup_Name) from startups  group by SubVertical HAVING COUNT(Startup_Name)=(select MAX(mycount) from (select COUNT(Startup_Name) mycount from  startups where SubVertical!='nan' group by SubVertical))")
highestGrowthstartup.write.save(path='/tmp/output/highestgrowthstart',format='csv',mode='Overwrite')



#Which SubVertical had the highest growth(in funding) over the years?
highestGrowthFunding=spark.sql("select SubVertical, SUM(CAST(regexp_replace(Amount_in_USD, '[^0-9]', '') AS BIGINT)) totalfund from startups group by SubVertical HAVING SUM(CAST(regexp_replace(Amount_in_USD, '[^0-9]', '') AS BIGINT))=(select MAX(totalamount) from (select (SUM(CAST(regexp_replace(Amount_in_USD, '[^0-9]*', '') AS BIGINT))) totalamount from startups where SubVertical!='nan' group by SubVertical))")
highestGrowthFunding.write.save(path='/tmp/output/highestgrowthfunding',format='csv',mode='Overwrite')
